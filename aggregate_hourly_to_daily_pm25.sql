DROP TABLE dev.airdailypm25;

CREATE TABLE dev.AIRDAILYPM25
(
   STATION_ID       VARCHAR (6),
   reading_date     DATE,
   reading_min      SMALLINT,
   reading_max      SMALLINT,
   reading_avg      DECIMAL (28, 10),
   reading_count    SMALLINT   
);

INSERT INTO AIRDAILYPM25 (station_id,
                          reading_date,
                          reading_min,
                          reading_max,
                          reading_avg,
                          reading_count)
   SELECT station_id,
          TO_DATE (YYYY || MM || DD, 'YYYYMMDD') AS READING_DATE,
          min (reading),
          max (reading),
          avg (reading),
          count (reading)
   FROM airhourlypm25_cleaned
   GROUP BY station_id,
            yyyy,
            mm,
            dd;