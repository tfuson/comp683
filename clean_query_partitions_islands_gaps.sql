SELECT    'update airhourlypm25_cleaned set reading = null where datetime >= '''
       || start_date
       || ''' AND datetime <= '''
       || end_date
       || ''' AND pollutant_code = '''
       || pollutant_code
       || ''' AND station_id = '''
       || station_id
       || ''''
FROM (SELECT grp,
             count (reading) AS duplicates,
             reading,
             max (pollutant_code) AS pollutant_code,
             max (station_id) AS station_id,
             MIN (datetime) AS start_date,
             MAX (datetime) AS end_date
      FROM (SELECT pollutant_code,
                   station_id,
                   datetime,
                   reading,
                     ROW_NUMBER ()
                        OVER (ORDER BY pollutant_code, station_id, datetime)
                   - ROW_NUMBER ()
                     OVER (PARTITION BY reading
                           ORDER BY pollutant_code, station_id, datetime) AS grp
            FROM airhourlypm25_cleaned) AS t
      GROUP BY grp, reading
      ORDER BY count (reading) DESC) AS d
WHERE duplicates > 23;

create table airhourlypm25_cleaned as select * from airhourlypm25;

create table airhourlyo3_cleaned as select * from airhourlyo3;

DROP TABLE airhourlypm25_1995_bak;

CREATE TABLE airhourlypm25_1995_bak
AS
   SELECT * FROM airhourlypm25_1995;

SELECT *
FROM airhourlypm25_1995_bak
ORDER BY pollutant_code, station_id, datetime;

drop table airdailypm25;
