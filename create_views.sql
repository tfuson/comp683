CREATE OR REPLACE VIEW airo3_avg_2010
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailyo3
   WHERE EXTRACT (YEAR FROM reading_date) = 2010;

SELECT * FROM dev.airo3_avg_2010;

CREATE OR REPLACE VIEW airo3_avg_2013
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailyo3
   WHERE EXTRACT (YEAR FROM reading_date) = 2013;

CREATE OR REPLACE VIEW airo3_avg_2016
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailyo3
   WHERE EXTRACT (YEAR FROM reading_date) = 2016;


CREATE OR REPLACE VIEW airpm25_avg_2010
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailypm25
   WHERE EXTRACT (YEAR FROM reading_date) = 2010;
   
CREATE OR REPLACE VIEW airpm25_avg_2013
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailypm25
   WHERE EXTRACT (YEAR FROM reading_date) = 2013;
   
CREATE OR REPLACE VIEW airpm25_avg_2016
AS
   SELECT station_id, reading_date, reading_avg
   FROM airdailypm25
   WHERE EXTRACT (YEAR FROM reading_date) = 2016;