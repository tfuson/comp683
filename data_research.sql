SELECT *
FROM (SELECT station_id,
             YYYY,
             mm,
             dd,
             reading,
             count (reading)
      FROM dev.airhourlypm25_nulls_2010
      GROUP BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd,
               reading
      ORDER BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd,
               hh) AS READING_COUNTS
WHERE reading_counts.count > 23;


SELECT pollutant_code,
       station_id,
       yyyy,
       mm,
       dd,
       hh,
       reading,
       lag (reading, 1) OVER (ORDER BY pollutant_code, station_id, datetime),
       (first_value (airhourlypm25_nulls_2010)
        OVER (PARTITION BY pollutant_code, station_id, reading
              ORDER BY pollutant_code, station_id, datetime)).*,
       (last_value (airhourlypm25_nulls_2010)
        OVER (PARTITION BY pollutant_code, station_id, reading
              ORDER BY pollutant_code, station_id, datetime)).*
FROM dev.airhourlypm25_nulls_2010
WHERE reading IS NOT NULL
ORDER BY pollutant_code, station_id, datetime
FETCH FIRST 1000 ROWS ONLY;

SELECT reading,
       pollutant_code,
       station_id,
       yyyy,
       mm,
       dd,
       hh
FROM airhourlypm25_nulls_2010
GROUP BY reading,
         pollutant_code,
         station_id,
         yyyy,
         mm,
         dd,
         hh;


SELECT reading,
       pollutant_code,
       station_id,
       datetime,
       row_number () OVER (ORDER BY pollutant_code, station_id, datetime)
FROM airhourlypm25_nulls_2010
WHERE reading IS NOT NULL
GROUP BY reading,
         pollutant_code,
         station_id,
         datetime
ORDER BY pollutant_code, station_id, datetime;


SELECT pollutant_code,
       station_id,
       datetime,
       reading,
       Row_Number ()
       OVER (PARTITION BY reading, grp
             ORDER BY pollutant_code, station_id, datetime)
FROM (SELECT a.*,
             (  datetime
              -   row_number ()
                     OVER (ORDER BY pollutant_code, station_id, datetime)
                * INTERVAL '1 hour') AS grp
      FROM airhourlypm25_nulls_2010 a) a
ORDER BY pollutant_code, station_id, datetime;
