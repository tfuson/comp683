CREATE TABLE dev.AIRDAILYPM25
(
   STATION_ID      VARCHAR (6),
   reading_date    DATE,
   reading_avg     DECIMAL (28, 10)
);

DROP TABLE dev.airdailypm25;

INSERT INTO AIRDAILYPM25 (station_id, reading_date, reading_avg)
   SELECT station_id,
          TO_DATE (YYYY || MM || DD, 'YYYYMMDD') AS READING_DATE,
          avg (reading)
   FROM airhourlypm25_v2
   GROUP BY station_id,
            yyyy,
            mm,
            dd;

SELECT *
FROM airdailypm25
WHERE EXTRACT (YEAR FROM reading_date) = 2010;

SELECT DISTINCT station_id
FROM airdailypm25
WHERE EXTRACT (YEAR FROM reading_date) = 2010
ORDER BY station_id;


SELECT reading_date::DATE
FROM generate_series (TIMESTAMP '2010-01-01',
                      TIMESTAMP '2010-12-31',
                      INTERVAL '1 day') AS READING_DATE;



SELECT station_id, reading_date::DATE, null as reading_avg
FROM generate_series (TIMESTAMP '2010-01-01',
                      TIMESTAMP '2010-12-31',
                      INTERVAL '1 day') AS READING_DATE
     NATURAL JOIN (SELECT DISTINCT station_id
                   FROM airdailypm25
                   WHERE EXTRACT (YEAR FROM reading_date) = 2010
                   ORDER BY station_id) AS STATIONS;
                   
                   
select 'a', 100 UNION select 'a', null;