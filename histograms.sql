SELECT *
FROM (SELECT station_id,
             YYYY,
             mm,
             dd,
             reading,
             count (reading)
      FROM dev.airhourlypm25_nulls_2010
      WHERE reading <> 0
      GROUP BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd,
               reading
      ORDER BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd) AS READING_COUNTS
ORDER BY count DESC;

select * from dev.airhourlypm25_nulls_2010 where station_id = '129202' and YYYY='2010' and MM='10' and DD='17';

select count(*) from airhourlypm25;


select count as repeat_char_size, count(count) as number_of_occurances from (
SELECT count
FROM (SELECT station_id,
             YYYY,
             mm,
             dd,
             reading,
             count (reading)
      FROM dev.airhourlypm25_nulls_2010
      WHERE reading = 1
      GROUP BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd,
               reading
      ORDER BY station_id,
               pollutant_code,
               yyyy,
               mm,
               dd) AS READING_COUNTS
ORDER BY count DESC) AS HISTOGRAM GROUP BY COUNT order by histogram.count desc;