select * from airhourlyo3 where reading = '-999' fetch first 100 rows only;

select * from airdaily03 fetch first 100 rows only;

create table dev.airhourlyo3_nulls as select * from dev.airhourlyo3;

update dev.airhourlyo3_nulls set reading = null where reading = -999;

commit;

CREATE TABLE dev.AIRDAILYO3_NULLS
(
   STATION_ID      VARCHAR (6),
   reading_date    DATE,
   reading_min     smallint,
   reading_max     smallint,
   reading_avg     DECIMAL (28, 10),
   reading_count   smallint,
   reading_sum     smallint
);

DROP TABLE dev.airdailyo3_nulls;

INSERT INTO AIRDAILYo3_nulls (station_id, reading_date, reading_min, reading_max, reading_avg, reading_count, reading_sum)
   SELECT station_id,
          TO_DATE (YYYY || MM || DD, 'YYYYMMDD') AS READING_DATE,
          min (reading),
          max (reading),          
          avg (reading),
          count (reading),
          sum (reading)
   FROM airhourlyo3_nulls
   GROUP BY station_id,
            yyyy,
            mm,
            dd;