SELECT *
FROM airhourlypm25
WHERE reading = '-999'
FETCH FIRST 100 ROWS ONLY;

SELECT *
FROM airdailypm25
FETCH FIRST 100 ROWS ONLY;

CREATE TABLE dev.airhourlypm25_nulls
AS
   SELECT * FROM dev.airhourlypm25;

UPDATE dev.airhourlypm25_nulls
SET reading = NULL
WHERE reading = -999;

CREATE TABLE dev.airhourlypm25_nulls_2010
AS
   SELECT *
   FROM dev.airhourlypm25_nulls
   WHERE YYYY = '2010';

SELECT *
FROM dev.airhourlypm25_nulls_2010
FETCH FIRST 100 ROWS ONLY;

CREATE TABLE dev.AIRDAILYPM25_NULLS
(
   STATION_ID       VARCHAR (6),
   reading_date     DATE,
   reading_min      SMALLINT,
   reading_max      SMALLINT,
   reading_avg      DECIMAL (28, 10),
   reading_count    SMALLINT,
   reading_sum      SMALLINT
);

DROP TABLE dev.airdailypm25_nulls;

INSERT INTO AIRDAILYPM25_nulls (station_id, reading_date, reading_avg)
   SELECT station_id,
          TO_DATE (YYYY || MM || DD, 'YYYYMMDD') AS READING_DATE,
          min (reading),
          max (reading),
          avg (reading),
          count (reading),
          sum (reading)
   FROM airhourlypm25_nulls
   GROUP BY station_id,
            yyyy,
            mm,
            dd;

SELECT *
FROM airdailypm25_nulls
WHERE station_id = '090302'
ORDER BY reading_date
FETCH FIRST 1000 ROWS ONLY;

SELECT *
FROM airdailypm25
WHERE station_id = '090302'
ORDER BY reading_date
FETCH FIRST 1000 ROWS ONLY;

SELECT *
FROM airhourlypm25_nulls
WHERE YYYY = '2010' AND MM = '01' AND DD = '03'
FETCH FIRST 1000 ROWS ONLY;

SELECT *
FROM airhourlypm25_nulls
WHERE YYYY = '2010'
FETCH FIRST 1000 ROWS ONLY;

SELECT *
FROM airpm25
WHERE dateyyymmdd = '20100103' AND pollutant_code = '015';