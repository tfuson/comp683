DROP TABLE dev.AIRPM25;

TRUNCATE dev.airpm25;

CREATE TABLE dev.AIRPM25 (POLLUTANT_CODE VARCHAR(10),
STATION_ID VARCHAR(10),
DATEYYYMMDD VARCHAR(10),
DAY_AVG DECIMAL (28,10),
DAY_MIN DECIMAL (28,10),
DAY_MAX DECIMAL (28,10),
HOUR1 DECIMAL (28,10),
HOUR2 DECIMAL (28,10),
HOUR3 DECIMAL (28,10),
HOUR4 DECIMAL (28,10),
HOUR5 DECIMAL (28,10),
HOUR6 DECIMAL (28,10),
HOUR7 DECIMAL (28,10),
HOUR8 DECIMAL (28,10),
HOUR9 DECIMAL (28,10),
HOUR10 DECIMAL (28,10),
HOUR11 DECIMAL (28,10),
HOUR12 DECIMAL (28,10),
HOUR13 DECIMAL (28,10),
HOUR14 DECIMAL (28,10),
HOUR15 DECIMAL (28,10),
HOUR16 DECIMAL (28,10),
HOUR17 DECIMAL (28,10),
HOUR18 DECIMAL (28,10),
HOUR19 DECIMAL (28,10),
HOUR20 DECIMAL (28,10),
HOUR21 DECIMAL (28,10),
HOUR22 DECIMAL (28,10),
HOUR23 DECIMAL (28,10),
HOUR24 DECIMAL (28,10));