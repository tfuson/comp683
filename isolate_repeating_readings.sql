SELECT grp,
       count (reading),
       reading,
       max(pollutant_code),
       max(station_id),
       MIN (datetime) AS start_date,
       MAX (datetime) AS end_date
FROM (SELECT pollutant_code,
             station_id,
             datetime,
             reading,
               ROW_NUMBER ()
                  OVER (ORDER BY pollutant_code, station_id, datetime)
             - ROW_NUMBER ()
               OVER (PARTITION BY reading
                     ORDER BY pollutant_code, station_id, datetime) AS grp
      FROM airhourlypm25_1995) AS t
GROUP BY grp, reading order by count(reading) desc;


SELECT pollutant_code,
       station_id,
       datetime,
       reading,
         ROW_NUMBER () OVER (ORDER BY pollutant_code, station_id, datetime)
       - ROW_NUMBER ()
         OVER (PARTITION BY reading
               ORDER BY pollutant_code, station_id, datetime) AS grp
FROM airhourlypm25_1995;


SELECT datetime,
       reading,
         ROW_NUMBER () OVER (ORDER BY datetime)
       - ROW_NUMBER () OVER (PARTITION BY reading ORDER BY datetime) AS grp
FROM airhourlypm25_1995;