DROP TABLE dev.AIRHOURLYO3;

CREATE TABLE dev.AIRHOURLYO3
(
   POLLUTANT_CODE    VARCHAR (3),
   STATION_ID        VARCHAR (6),
   DATETIME          TIMESTAMP,
   YYYY              VARCHAR (4),
   MM                VARCHAR (2),
   DD                VARCHAR (2),
   HH                VARCHAR (2),
   READING           SMALLINT
);

SELECT * FROM dev.airhourlyo3 --FETCH FIRST 10 ROWS ONLY;

TRUNCATE dev.airhourlyo3;

INSERT INTO airhourlyo3 (pollutant_code,
                         station_id,
                         datetime,
                         yyyy,
                         mm,
                         dd,
                         hh,
                         reading)
   (SELECT POLLUTANT_CODE,
           STATION_ID,
           TO_TIMESTAMP(dateyyymmdd || '23', 'YYYYMMDDHH24') as DATETIME,
           SUBSTRING (dateyyymmdd FROM 1 FOR 4) AS YYYY,
           SUBSTRING (dateyyymmdd FROM 5 FOR 2) AS MM,
           SUBSTRING (dateyyymmdd FROM 7 FOR 2) AS DD,
           '24' AS HH,
           CAST (HOUR24 AS SMALLINT) AS READING
    FROM dev.airo3);

--FETCH FIRST 10 ROWS ONLY);

select max(reading), min(reading) from airhourlyo3;

create table dev.airo3_backup as select * from dev.airo3;

SELECT POLLUTANT_CODE,
           STATION_ID,
           TO_TIMESTAMP(dateyyymmdd || '00', 'YYYYMMDDHH24') as DATETIME,
           SUBSTRING (dateyyymmdd FROM 1 FOR 4) AS YYYY,
           SUBSTRING (dateyyymmdd FROM 5 FOR 2) AS MM,
           SUBSTRING (dateyyymmdd FROM 7 FOR 2) AS DD,
           '01' AS HH,
           CAST (HOUR1 AS SMALLINT) AS READING
           FROM dev.airo3
           FETCH FIRST 10 ROWS ONLY;