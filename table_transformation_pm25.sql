DROP TABLE dev.AIRHOURLYPM25;

CREATE TABLE dev.AIRHOURLYPM25
(
   POLLUTANT_CODE    VARCHAR (3),
   STATION_ID        VARCHAR (6),
   DATETIME          TIMESTAMP,
   YYYY              VARCHAR (4),
   MM                VARCHAR (2),
   DD                VARCHAR (2),
   HH                VARCHAR (2),
   READING           SMALLINT
);

SELECT * FROM dev.airhourlypm25; --FETCH FIRST 10 ROWS ONLY;

DROP table dev.airhourly;

select * from dev.airhourly fetch first 10 rows only;

INSERT INTO airhourlypm25 (pollutant_code,
                         station_id,
                         datetime,
                         yyyy,
                         mm,
                         dd,
                         hh,
                         reading)
   (SELECT POLLUTANT_CODE,
           STATION_ID,
           TO_TIMESTAMP(dateyyymmdd || '23', 'YYYYMMDDHH24') as DATETIME,
           SUBSTRING (dateyyymmdd FROM 1 FOR 4) AS YYYY,
           SUBSTRING (dateyyymmdd FROM 5 FOR 2) AS MM,
           SUBSTRING (dateyyymmdd FROM 7 FOR 2) AS DD,
           '24' AS HH,
           CAST (HOUR24 AS SMALLINT) AS READING
    FROM dev.airpm25);

--FETCH FIRST 10 ROWS ONLY);

select max(reading), min(reading) from airhourlypm25;

create table dev.airpm25_backup as select * from dev.airpm25;

SELECT POLLUTANT_CODE,
           STATION_ID,
           TO_TIMESTAMP(dateyyymmdd || '00', 'YYYYMMDDHH24') as DATETIME,
           SUBSTRING (dateyyymmdd FROM 1 FOR 4) AS YYYY,
           SUBSTRING (dateyyymmdd FROM 5 FOR 2) AS MM,
           SUBSTRING (dateyyymmdd FROM 7 FOR 2) AS DD,
           '01' AS HH,
           CAST (HOUR1 AS SMALLINT) AS READING
           FROM dev.airpm25
           FETCH FIRST 10 ROWS ONLY;