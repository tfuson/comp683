CREATE VIEW air1995
AS
   SELECT reading,
          pollutant_code,
          station_id,
          datetime,
          row_number () OVER (ORDER BY pollutant_code, station_id, datetime)
   FROM airhourlypm25_1995
   WHERE reading IS NOT NULL
   GROUP BY reading,
            pollutant_code,
            station_id,
            datetime
   ORDER BY pollutant_code, station_id, datetime;

SELECT * FROM air1995;

DROP VIEW air1995;


SELECT reading,
       pollutant_code, station_id,
       MIN (datetime) AS min_datetime,
       MAX (datetime) AS max_datetime
FROM (SELECT pollutant_code, station_id, datetime,
             reading,
               ROW_NUMBER () OVER (ORDER BY pollutant_code, station_id, datetime)
             - ROW_NUMBER () OVER (PARTITION BY reading ORDER BY pollutant_code, station_id, datetime) AS grp
      FROM airhourlypm25_1995) AS t
GROUP BY reading, grp;