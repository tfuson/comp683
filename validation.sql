SELECT  count (dateyyy), dateyyy || 'O3.fil'
FROM (SELECT SUBSTRING (dateyyymmdd FROM 1 FOR 4) AS dateyyy FROM airo3)
     AS airo3_new
GROUP BY dateyyy
ORDER BY dateyyy ASC;

SELECT  count (dateyyy), dateyyy || 'PM25.fil'
FROM (SELECT SUBSTRING (dateyyymmdd FROM 1 FOR 3) AS dateyyy FROM airpm25)
     AS airpm25_new
GROUP BY dateyyy
ORDER BY dateyyy ASC;
